#!/usr/bin/env python3

from nerds import Account
from nerds import Customer
from nerds import MoneyMarket

stringWelcome = "Welcome to Bank of Nerds\n"
stringTransaction = "1) Complete Transaction\n"
stringAdd = "2) Add New Customer\n"
stringNewAcct = "3) Add New Account to Current Customer\n"
stringView = "4) View Balance\n"
stringexit = "5) Exit"
stringSelect = "Please Make a Selection:"
stringMainMenu = "".join((stringWelcome, stringTransaction, stringAdd,
                          stringNewAcct, stringView, stringexit))
stringPos = "Input must be a positive number."
stringComplete = "Complete transaction with another account?(y/n)"
stringCont = "Complete another transaction?(y/n)"


def pause():
    """Function to pause the program to allow customer to read information
       dispalyed on the screen, before making any further actions.
       -Input parameters: N\A
       -Return Value: None
    """
    input("Press Enter to Return to Menu...")


def customerList(bankCustomers):
    # Ensuring there are customers in the customer list
    if len(bankCustomers) == 0:
        print("No Customers are Members of Bank of Nerds")
        return
    #
    for index, customer in enumerate(bankCustomers, 1):
        print(index, ") ", sep="", end="")
        print(customer)
    who = input("Select:")
    cust = bankCustomers[int(who) - 1]
    print(cust)
    return cust


def view(bankCustomers):
    """Function to view the balances of all the accounts for a particular
       customer
       -Input parameters: bankCustomers (list of all current customers)
       -Return Value: None
    """
    customer = customerList(bankCustomers)
    customer.printAccounts()
    pause()


def currencyExch():
    """Function to determine what currency the user would like to
       work index
       -Input parameters = N\A
       -Return Value = exchange rate
    """
    # Exchange rates obtained 10 March 2017 at 10:23 am
    # exchange rate is for $1 USD to transfer into the currency selected
    currency = {"1": 1, "2": 561.678, "3": .94116, "4": .82264,
                "5": 1.34611, "6": 6.91239, "7": 19.6031}
    exchange = None
    while not exchange:
        print("1) $ USD\n2) \u20A1 Costa Rican Colon\n3) \u00A3 Euros")
        print("4) \u20AC British Pounds\n5) $ Canadian Dollars")
        print("5) \u00A5Chinese Yen\n6) \u20B1 Mexican Peso")
        cur = input("Chose a currency to bank with:")
        exchange = currency.get(cur)
        if not exchange:
            default()
    return exchange


def inputInt(stringValue):
    """Fucntion used to ensure user input is an integer
       -Input parameters = stringValue used to ask for the numbers
       -Return Value = integer use input
    """
    while True:
        try:
            number = input(stringValue)
            int(number)
            break
        except ValueError:
            print("Please enter an integer")
    return number


def contTransaction(stringValue):
    """Fucntion to determine if user would liek to continue
       -Input parameters = N\A
       -Return Value = boolean
    """
    while True:
        more = input(stringValue)
        more = more.lower()
        if more == "y":
            return True
        elif more == "n":
            return False
        else:
            default()


def transAmount(stringText):
    """Function to determine the amount of the transaction in question
       -Input parameters = string to be displayed on prompt
       -Return Value = amount to be used in the transaction
    """
    while True:
        try:
            amount = float(input(stringText))
            if amount <= 0:
                print(stringPos)
                continue
            break
        except ValueError:
            print(stringPos)
    return amount


def transaction(bankCustomers):
    """Function to withdrawl or deposit into an acount
       -Input parameters:  bankCustomers (list of all current customers)
       -Return Value: None
    """
    cust = customerList(bankCustomers)

    while True:
        cust.printAccounts()
        # Test to ensure user input is one of the valid account numbers
        while True:
            acctNum = inputInt("Select an account Number:")
            acctNum = int(acctNum)
            accountNum = cust.accounts.get(acctNum)
            # Ensure the user actual inputs an interger
            if accountNum:
                # break if function returned an actual account number
                break
            default()

        limit = False
        # Ensuring MMF account had not reached transaction limit
        if accountNum.cat == "MMF":
            limit = accountNum.usedTrans()
            if limit:
                while True:
                    # Asking if customer still wishes to continue
                    if contTransaction(stringComplete):
                        break
                    else:
                        return
                # Customer would like to use a different acount to complete
                # the transaction
                continue
            accountNum.addTransCount()
        transaction = None
        while not transaction:
            try:
                transType = {1: "withdrawl", 2: "deposit"}
                print("1)Withdrawl\n2)Deposit")
                transaction = input("Select Transaction:")
                transaction = int(transaction)
                if transaction < 1 or transaction > 2:
                    transaction = None
                else:
                    transaction = transType.get(transaction)
            except ValueError:
                default()
                transaction = None
        # Testing if account is 401(k) and customer is proper
        # age to withdrawl
        if not accountNum.isproperAge(cust.age):
            if contTransaction(stringComplete):
                # Customer would like to use a different acount to complete
                # the transaction
                break
            else:
                return

        exchange = currencyExch()
        # Test to enusure only positive numbers
        amount = transAmount("Transaction Amount:")

        amount = amount / exchange
        # Change to dict menuDict
        if transaction == "withdrawl":
            accountNum.withdrawl(amount)
        else:
            accountType.deposit(amount)

        # Print receipt for customer transaction to show new balance
        print("New Account Balance:\n", accountNum, sep="")

        while True:
            if contTransaction(stringCont):
                # Customer would like to make another transaction
                break
            else:
                return


def makeAccount():
    """Function used to create a new account1
       -Input parameters = N\A
       -Return Value = new instance of class Account
    """
    accountType = {"1": "Savings", "2": "Checking", "3": "401(k)",
                   "4": "MMF"}
    accttype = None
    # What type of accoutn are we opening
    while not accttype:
        print("1) Savings\n2) Checking\n3) 401(k)\n4) MMF\n5) Stop")
        choice = input("Account Type: ")
        if choice == "5":
            break
        accttype = accountType.get(choice)
        if not accttype:
            default()
    if choice == "5":
        return None

    # ensuring intial deposit is greater greater than 0
    initial = transAmount("Initial Deposit:")

    if choice == "4":
        account = MoneyMarket(accttype, int(initial))
    else:
        account = Account(accttype, int(initial))
    return account


def add(bankCustomers):
    """Function used to add new customer with arbitray number of accounts
       -Input parameters:  bankCustomers (list of all current customers)
       -Return Value: None
    """
    first = input("Enter First Name:")
    last = input("Enter Last Name:")
    name = " ".join((first, last))
    # Ensuring ag is input as a positive number
    age = 0
    while age <= 0:
        try:
            age = input("Enter Age:")
            age = int(age)
            if age <= 0:
                print(stringPos)
        except ValueError:
            default()
            print(stringPos)
            age = 0

    newCustomer = Customer(name, age)

    # Allow customer to open multiple accounts
    while True:
        account = makeAccount()
        if not account:
            break
        newCustomer.addAccount(account)
        print("New Account:")
        print(account)

    # When all accounts have been created add customer to bank's list
    # of customers
    bankCustomers.append(newCustomer)


def newAccount(bankCustomers):
    """Fucntion to add an account to an existing customer
       -Input parameters:  bankCustomers (list of all current customers)
       -Return Value: None
    """
    customer = customerList(bankCustomers)
    account = makeAccount()
    customer.addAccount(account)
    print("New Account:")
    print(account)


def exitProgram(bankCustomers):
    """Fucntion used to exit program gracefull
       -Input parameters:  bankCustomers (list of all current customers)
       -Return Value: None
    """
    # Input needed to have proper working
    print("Bank of Nerds has liquidated all assets, have a good day.")
    exit()


def default(*bankCustomers):
    """Fucntion used to display generic invalid input"""
    # Function needs to be able to accept an agrument because of
    # the main menu calling this function and the main menu having to
    # pass the customer list to every other function.
    print("Input not a valid choice\u203D")


def main():
    bankCustomers = []
    # First Bank of Nerds Welcome screen
    while True:
        mainMenu = {"1": transaction, "2": add, "3": newAccount,
                    "4": view, "5": exitProgram}
        print(stringMainMenu)
        choice = input(stringSelect)
        mainMenu.get(choice, default)(bankCustomers)


if __name__ == "__main__":
    main()

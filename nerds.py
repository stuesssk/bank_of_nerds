#!/usr/bin/env python3


class Account:
    """Class used to store information and functions for accounts"""
    userId = 10000

    def __init__(self, category, balance):
        self.cat = category
        self.id = Account.userId
        self.balance = float("{0:.2f}".format(balance))
        Account.userId += 1

    def __str__(self):
        # Ensuring proper tabed formatting and 2 decimal places
        # on balance
        msg = "#{0} Type:{1:8}  Balance:${2:.2f} USD"
        return msg.format(self.id, self.cat, self.balance)

    def withdrawl(self, amount):
        """Function to withdrawl money from an account"""
        # Check to ensure transaction will not bring balance below zero
        if self.balance + amount < 0:
            print("Transaction will cause negative balance!")
            return False
        # If deposit amount is positive, if withdrawl amount is negative
        self.balance -= amount
        return True

    def deposit(self, amount):
        """Function to deposit money into an account"""
        self.balance += amount
        return True

    def isproperAge(self, age):
        """Function to check if customer is proper age to withdrawl
           from a 401(k)
        """
        if age < 67 and self.cat == "401k":
            print("Minimum age has not been reached for 401k withdrawl")
            return False
        return True


class MoneyMarket(Account):
    """Class that is an extension of the Account Class"""
    def __init__(self, category, balance):
        super().__init__(category, balance)
        # Money Market Funds need to track number of transactions and
        # only allow 2 per month
        self.numTrans = 0

    def usedTrans(self):
        """Function to check if monthly transactions has been reached"""
        if self.numTrans >= 2:
            print("Monthly Transactions have already been used")
            return True
        return False

    def addTransCount(self):
        """Function to increment the accounts number of transactions"""
        self.numTrans += 1


class Customer:
    """Class used to store infromation and fruntions for customers"""
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.accounts = {}

    def __str__(self):
        msg = "Name:{0}\tAge:{1}"
        return msg.format(self.name, self.age)

    def addAccount(self, account):
        """Fucntion to add an account to the customer's account dictionary"""
        self.accounts[account.id] = (account)

    def printAccounts(self):
        """Function to print a customer's accounts"""
        for account in self.accounts:
            print(self.accounts.get(account))


if __name__ == "__main__":
    pass
